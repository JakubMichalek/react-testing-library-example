function uniq() {
	let id = 0
	return () => 'i'+ id++
}

const getUniqueId = uniq()

export { getUniqueId as uniq }

export default function reducer(
	state = [],
	action
) {
	switch (action.type) {
		case 'NEW_TODO':
			return state.concat({
				id: getUniqueId(),
				task: action.value,
				completed: false
			})
		case 'TOGGLE_TODO':
			return state.map(todo => {
				if (todo.id !== action.id) {
					return todo
				}
				return { ...todo, completed: !todo.completed }
			})
		case 'REMOVE_TODO':
			return state.filter(({ id }) => id !== action.id)
		default:
			return state
	}
}
