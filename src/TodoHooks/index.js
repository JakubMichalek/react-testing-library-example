import React, { useReducer } from 'react'
import reducer, { uniq } from './reducer'
import Context from './Context'
import NewTodo from './NewTodo'
import Todos from './Todos'

const initialState = [
	{ id: uniq(), task: 'Prepare', completed: true },
	{ id: uniq(), task: 'Commit', completed: false },
	{ id: uniq(), task: 'Deploy', completed: false },
]

export default function Todo() {
	return <Context.Provider value={useReducer(reducer, initialState)}>
		<NewTodo />
		<Todos />
	</Context.Provider>
}
