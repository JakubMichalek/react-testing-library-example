import React, { useContext } from 'react'
import Context from './Context'
import './Todos.css'

export default function Todos() {
	const [todos, dispatch] = useContext(Context)
	const remove = id => window.confirm('Really?') && dispatch({ type: 'REMOVE_TODO', id })

	if (todos.length === 0) {
		return <p className="todos todos--empty">No work left. Good job.</p>
	}

	return <ul className="todos">
		{todos.map(({ id, task, completed }, index) => (
			<li key={id} className="todos__todo">
				<button
					className="todos__status"
					onClick={() => dispatch({ type: 'TOGGLE_TODO', id })}
					data-testid={`toggle-${index}`}
				>
					{completed ? '✓' : '×'}
				</button>
				<span className="todos__text">{task}</span>
				<span className="todos__actions">
					<button onClick={() => remove(id)}>Remove</button>
				</span>
			</li>
		))}
	</ul>
}
