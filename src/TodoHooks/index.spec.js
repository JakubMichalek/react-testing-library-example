import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import 'jest-dom/extend-expect'

import Todo from './index'

describe('TodoClasses', () => {
	it('should have default state', () => {
		const { getByText } = render(<Todo />)

		getByText('Prepare')
		getByText('Commit')
		getByText('Deploy')
	})

	it('should have default state', () => {
		const { getByTestId } = render(<Todo />)
		const toggle = getByTestId('toggle-1')

		expect(toggle).toHaveTextContent('×')

		fireEvent.click(toggle)

		expect(toggle).toHaveTextContent('✓')
	})
})
