import React, { useContext, useCallback } from 'react'
import Context from './Context'
import './NewTodo.css'

export default function NewTodo() {
	const [_, dispatch] = useContext(Context)
	const onSubmit = useCallback((event) => {
		dispatch({
			type: 'NEW_TODO',
			value: event.target.elements.task.value
		})
		event.preventDefault()
		event.target.reset()
	}, [dispatch])

	return <form onSubmit={onSubmit} className="new-todo">
		<header className="new-todo__header">Add new todo</header>
		<div className="new-todo__control">
			<label className="new-todo__label">
				Task:
				<input type="text" name="task" required className="new-todo__input" />
			</label>
			<input type="submit" value="Create" className="new-todo__submit" />
		</div>
	</form>
}
