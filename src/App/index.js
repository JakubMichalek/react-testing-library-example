import React from 'react'
import Todo from '../TodoHooks'
// import Todo from '../TodoClasses'
import './index.css'

function App() {
  return (
    <div className="app">
      <header className="header">
        IT Economia TODO App
      </header>
      <Todo />
    </div>
  )
}

export default App
