import React from 'react'
import './Todos.css'

export default class Todos extends React.PureComponent {

	onRemove(id) {
		window.confirm('Really?') && this.props.onRemove(id)
	}

	render() {
		const { todos, onToggle } = this.props

		if (todos.length === 0) {
			return <p className="todos todos--empty">No work left. Good job.</p>
		}

		return <ul className="todos">
			{todos.map(({ id, task, completed }, index) => (
				<li key={id} className="todos__todo">
					<button
						className="todos__status"
						onClick={() => onToggle(id)}
						data-testid={`toggle-${index}`}
					>
						{completed ? '✓' : '×'}
					</button>
					<span className="todos__text">{task}</span>
					<span className="todos__actions">
						<button onClick={() => this.onRemove(id)}>Remove</button>
					</span>
				</li>
			))}
		</ul>
	}
}
