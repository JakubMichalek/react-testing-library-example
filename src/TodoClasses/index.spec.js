import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Todo from './index'

Enzyme.configure({ adapter: new Adapter() })

describe('TodoClasses', () => {
	it('should have default state', () => {
		const app = shallow(<Todo />)

		expect(app.state('todos')).toEqual([
			{ id: expect.any(String), task: 'Prepare', completed: true },
			{ id: expect.any(String), task: 'Commit', completed: false },
			{ id: expect.any(String), task: 'Deploy', completed: false },
		])
	})

	it('should mark todo as completed', () => {
		const app = shallow(<Todo />)
		app.instance().onToggle(app.state('todos')[1].id)

		expect(app.state('todos')).toEqual([
			{ id: expect.any(String), task: 'Prepare', completed: true },
			{ id: expect.any(String), task: 'Commit', completed: true },
			{ id: expect.any(String), task: 'Deploy', completed: false },
		])
	})
})
