import React from 'react'
import './NewTodo.css'

export default class NewTodo extends React.PureComponent {

	onSubmit(event) {
		this.props.onAdd(event.target.elements.task.value)
		event.preventDefault()
		event.target.reset()
	}

	render() {

		return <form onSubmit={e => this.onSubmit(e)} className="new-todo">
			<header className="new-todo__header">Add new todo</header>
			<div className="new-todo__control">
				<label className="new-todo__label">
					Task:
					<input type="text" name="task" required className="new-todo__input" />
				</label>
				<input type="submit" value="Create" className="new-todo__submit" />
			</div>
		</form>
	}
}
