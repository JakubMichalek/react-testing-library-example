import React from 'react'
import uniq from './uniq'
import NewTodo from './NewTodo'
import Todos from './Todos'

const initialState = [
	{ id: uniq(), task: 'Prepare', completed: true },
	{ id: uniq(), task: 'Commit', completed: false },
	{ id: uniq(), task: 'Deploy', completed: false },
]

export default class Todo extends React.PureComponent {

	state = {
		todos: initialState
	}

	onAdd(task) {
		this.setState(state => ({
			...state,
			todos: state.todos.concat({
				id: uniq(),
				task,
				completed: false
			})
		}))
	}

	onToggle(id) {
		this.setState(state => ({
			...state,
			todos: state.todos.map(todo => {
				if (todo.id !== id) {
					return todo
				}
				return { ...todo, completed: !todo.completed }
			})
		}))
	}

	onRemove(id) {
		this.setState(state => ({
			...state,
			todos: state.todos.filter(todo => todo.id !== id)
		}))
	}

	render() {
		return <>
			<NewTodo onAdd={task => this.onAdd(task)} />
			<Todos
				todos={this.state.todos}
				onRemove={id => this.onRemove(id)}
				onToggle={id => this.onToggle(id)}
			/>
		</>
	}
}
